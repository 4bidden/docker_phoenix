FROM phusion/baseimage
MAINTAINER Claudiu Necula <claudiu.necula@gmail.com>

CMD ["/sbin/my_init"]
 
RUN locale-gen en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# update and install some software requirements
RUN apt-get update && apt-get upgrade -y && apt-get install -y curl wget git make build-essential puppet

RUN puppet module install puppetlabs/vcsrepo

# download and install Erlang package
RUN wget http://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb \
 && dpkg -i erlang-solutions_1.0_all.deb \
 && apt-get update
 

# install erlang from package
RUN apt-get install -y erlang erlang-ssl erlang-inets elixir && rm erlang-solutions_1.0_all.deb

# install elixir

ENV ELIXIR_VERSION="v1.5.1" \
	LANG=C.UTF-8

ENV PATH $PATH:/elixir/bin

ENV PHOENIX_VERSION 1.3.2

# install Phoenix from source with some previous requirements
RUN mix local.hex --force && mix local.rebar --force \
 && mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez --force
 
# install Node.js and NPM in order to satisfy brunch.io dependencies
# the snippet below is borrowed from the official nodejs Dockerfile
# https://registry.hub.docker.com/_/node/

ENV NODE_VERSION 6.3.0
ENV NPM_VERSION 3.10.6

RUN curl -SLO "http://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz" \
 && tar -xzf "node-v$NODE_VERSION-linux-x64.tar.gz" -C /usr/local --strip-components=1 \
 && rm "node-v$NODE_VERSION-linux-x64.tar.gz" \
 && npm install -g npm@"$NPM_VERSION" \
 && npm install -g brunch \
 && npm cache clear

RUN echo "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config
RUN mkdir /home/app
ADD ssh /home/app/.ssh
ADD ssh /root/.ssh
RUN chmod -R 600 /home/app/.ssh
RUN chmod -R 600 /root/.ssh

#Set production env
ENV MIX_ENV prod

ENV HOME /home/app/generic_app
ADD your_manifest.pp /root/your_manifest.pp
RUN puppet apply /root/your_manifest.pp
ADD update_app.sh /etc/my_init.d/00_update_app.sh
RUN mkdir /etc/service/your_app
ADD run_app.sh /etc/service/your_app/run

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 4000

