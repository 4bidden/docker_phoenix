vcsrepo { "/home/app/generic_app":
    ensure => latest,
    provider => git,
    source => 'git@bitbucket.org:4bidden/generic_phoenix.git',
    revision => 'master'
}
->
exec { "mix local.hex --force && mix local.rebar --force":
  environment => ["HOME=/home/app/generic_app","MIX_ENV=prod"],
  cwd     => "/home/app/generic_app",
  path    => ["/usr/local/sbin", "/usr/local/bin","/usr/sbin","/usr/bin","/sbin","/bin","/elixir/bin"]
}
->
exec { "mix deps.get --only prod --force":
  environment => ["HOME=/home/app/generic_app","MIX_ENV=prod"],
  cwd     => "/home/app/generic_app",
  path    => ["/usr/local/sbin", "/usr/local/bin","/usr/sbin","/usr/bin","/sbin","/bin","/elixir/bin"]
}
->
exec { "npm install":
  cwd     => "/home/app/generic_app/assets",
  path    => ["/usr/local/sbin", "/usr/local/bin","/usr/sbin","/usr/bin","/sbin","/bin"]
}
->
exec { "mix compile":
  environment => ["HOME=/home/app/generic_app","MIX_ENV=prod"],
  cwd     => "/home/app/generic_app",
  path    => ["/usr/local/sbin", "/usr/local/bin","/usr/sbin","/usr/bin","/sbin","/bin","/elixir/bin"]
}
->
exec { "brunch build --production":
  environment => ["HOME=/home/app/generic_app","MIX_ENV=prod"],
  cwd     => "/home/app/generic_app/assets",
  path    => ["/usr/local/sbin", "/usr/local/bin","/usr/sbin","/usr/bin","/sbin","/bin","/elixir/bin"]
}
->
exec { "mix phoenix.digest":
  environment => ["HOME=/home/app/generic_app","MIX_ENV=prod"],
  cwd     => "/home/app/generic_app",
  path    => ["/usr/local/sbin", "/usr/local/bin","/usr/sbin","/usr/bin","/sbin","/bin","/elixir/bin"]
}