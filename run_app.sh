#!/bin/bash

export HOME=/home/app/generic_app
cd $HOME
export PORT=4000
exec iex -S mix phx.server
